import axios from 'axios'

const state = {
    datalist: {
        data: {
            saleDB: [],
            newsDB: [],
            shirtDB: [],
            shoesDB: [],
            accessoriesDB: []
        }
    }
}

const getters = {

    saleData: state => state.datalist.data.saleDB,
    newsData: state => state.datalist.data.newsDB,
    shirtData: state => state.datalist.data.shirtDB,
    shoesData: state => state.datalist.data.shoesDB,
    accessoriesData: state => state.datalist.data.accessoriesDB,
    saleDataLength: state => state.datalist.data.saleDB.length,
    shoesDataLength: state => state.datalist.data.shoesDB.length,
    shirtDataLength: state => state.datalist.data.shirtDB.length,
    accessoriesDataLength: state => state.datalist.data.accessoriesDB.length,
    newsDataLength: state => state.datalist.data.newsDB.length,
}

const actions = {
    async getHomePage({ commit }) {
        try {
            const dataHomePage = await axios.get('http://localhost:3000/turborvipshop/home');
            console.log('alldata', dataHomePage);
            commit('SET_DATA_HOMEPAGE', dataHomePage.data)
        } catch (error) {
            console.log(error)
        }
    }
}

const mutations = {
    SET_DATA_HOMEPAGE(state, dataHomePage) {
        state.datalist = dataHomePage
    }
}

export default {
    state,
    mutations,
    getters,
    actions,
}
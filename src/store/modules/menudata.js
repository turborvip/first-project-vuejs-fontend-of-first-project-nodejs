import axios from 'axios'
const state = {
    menulist: [],
}

const getters = {
    maleMenuCategory: state => {
        return state.menulist.data.categoryMaleDB
    },
    femaleMenuCategory: state => state.menulist.data.categoryFeMaleDB,
    childCategory: state => state.menulist.data.categoryChildDB,

}

const actions = {
    async getMenu({ commit }) {
        try {
            const menu = await axios.get('http://localhost:3000/turborvipshop/menu');
            commit('SET_MENU', menu.data)
        } catch (error) {
            console.log(error)
        }
    }
}

const mutations = {
    SET_MENU(state, menu) {
        state.menulist = menu
    }
}

export default {
    state,
    mutations,
    getters,
    actions,
}
import Vue from 'vue'
import VueX from 'vuex'
// import axios from 'axios'

//import modules 
import menudata from './modules/menudata'
import homedata from './modules/homedata'

Vue.use(VueX);

const storeData = {
    modules: {
        menudata,
        homedata,
    },
    state: {

    },
    getters: {

    },
    actions: {

    },
    mutations: {

    },

}

const store = new VueX.Store(storeData)

export default store 
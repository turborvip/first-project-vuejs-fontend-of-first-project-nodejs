import Vue from 'vue'

import App from './App.vue'

import { BootstrapVue, IconsPlugin, BootstrapVueIcons } from "bootstrap-vue";

import store from './store/index_store';

import VueRouter from 'vue-router'
import { routes } from './router/index'

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css'
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(BootstrapVueIcons)

Vue.use(VueRouter);
Vue.config.productionTip = false

const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  render: h => h(App),
  store,
  router,
}).$mount('#app')

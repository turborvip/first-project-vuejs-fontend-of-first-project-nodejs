import Home from '../components/Home/Home.vue'

import Login from '../components/Login/Login.vue'

import Error from '../components/Error/Error.vue'

export
    const routes = [
        { path: '/', name: 'Home', component: Home },
        { path: '/login', name: 'Login', component: Login },
        { path: '/:pathMatch(.*)*', name: 'error', component: Error }
    ]
